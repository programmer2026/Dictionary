﻿namespace Dictionary
{
    partial class EditDictionaries
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.AddDictionary = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.deleteDictionary = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(13, 13);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // AddDictionary
            // 
            this.AddDictionary.Location = new System.Drawing.Point(14, 141);
            this.AddDictionary.Name = "AddDictionary";
            this.AddDictionary.Size = new System.Drawing.Size(75, 23);
            this.AddDictionary.TabIndex = 2;
            this.AddDictionary.Text = "Добавить";
            this.AddDictionary.UseVisualStyleBackColor = true;
            this.AddDictionary.Click += new System.EventHandler(this.AddDictionary_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(14, 40);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(258, 95);
            this.listBox1.TabIndex = 3;
            // 
            // deleteDictionary
            // 
            this.deleteDictionary.Location = new System.Drawing.Point(197, 141);
            this.deleteDictionary.Name = "deleteDictionary";
            this.deleteDictionary.Size = new System.Drawing.Size(75, 23);
            this.deleteDictionary.TabIndex = 4;
            this.deleteDictionary.Text = "Удалить";
            this.deleteDictionary.UseVisualStyleBackColor = true;
            this.deleteDictionary.Click += new System.EventHandler(this.DeleteDictionary_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(151, 13);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 1;
            // 
            // EditDictionaries
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 175);
            this.Controls.Add(this.deleteDictionary);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.AddDictionary);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EditDictionaries";
            this.Text = "Редактирование словарей";
            this.Load += new System.EventHandler(this.EditDictionaries_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button AddDictionary;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button deleteDictionary;
        private System.Windows.Forms.ComboBox comboBox2;
    }
}