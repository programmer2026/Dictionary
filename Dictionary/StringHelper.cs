﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    static class StringHelper
    {
        /// <summary>
        /// Возращает сгенерированное имя файла.
        /// </summary>
        public static string GenerateFileName(string fileName, string expansion)
        {
            char[] reservedChar = {'/', '\\', '?', '*', '|', '"', '>', '<', ' ', '.'};
            foreach (char c in reservedChar)
            {
                fileName = fileName.Replace(c.ToString(), "");
                expansion = expansion.Replace(c.ToString(), "");
            }
            return $"{fileName}.{expansion}";
        }
    }
}
