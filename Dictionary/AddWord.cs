﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class AddWord : Form
    {
        private DataGridView dataGridViewForm1;

        public AddWord()
        {
            InitializeComponent();
        }

        public AddWord(DataGridView dataGridView) : this()
        {
            dataGridViewForm1 = dataGridView;
        }

        private void AddWord_Load(object sender, EventArgs e)
        {
            if (MyDataBase.TestConnection())
            {
                MyDataBase.LoadColumnsInDataGridView("words", dataGridView1);
                // Удаляем колонку Category 
                dataGridView1.Columns.Remove(dataGridView1.Columns[dataGridView1.Columns.Count - 1]);
                // Добавляем одну строку в DataTable для того, что б в неё можно было записать новое слово или изменить текущее.
                dataGridView1.Rows.Add();
                DataGridViewHelper.HideColumn(dataGridView1, 0);
                MyDataBase.LoadCategoryInComboBoxColumn("SELECT * FROM categories;", DataGridViewHelper.AddingComboBoxColumn(dataGridView1, "category", 100, dataGridView1.ColumnCount));
            }
            else
            {
                MessageBox.Show("Вы должны выбрать словарь.");
                Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Мы передаем startIndexOfCell = 1, а не 0, потому что первый (0) столбец это id, 
            // а мы его не даём вводить пользователю
            if (!DataGridViewHelper.isThereEmptyCellInRow(dataGridView1, 0, 1))
            {
                string answer = MyDataBase.ExecuteQueryWithAnswer("SELECT MAX(id) FROM words;");
                int maxID = answer == string.Empty ? 0 : int.Parse(answer) + 1;

                MyDataBase.ExecuteQueryWithoutAnswer($"INSERT INTO words VALUES ({maxID}, {DataGridViewHelper.ToFromValues(dataGridView1, 0, 1, ", ")});");
                MessageBox.Show("Слово успешно добавлено!");

                MyDataBase.LoadDataToDataGridView("SELECT * FROM words;", dataGridViewForm1);
                DataGridViewHelper.HideColumnAndAddColumns(dataGridViewForm1, 0, dataGridViewForm1.ColumnCount,
                        DataGridViewHelper.AddingButtonColumn(dataGridViewForm1, "Change", "Изменить", 70, dataGridViewForm1.ColumnCount),
                        DataGridViewHelper.AddingButtonColumn(dataGridViewForm1, "Delete", "Удалить", 70, dataGridViewForm1.ColumnCount));

                Close();
            }
            else
            {
                MessageBox.Show("Вы не ввели все значения в таблицу!");
            }
        }

    }
}
