﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Dictionary
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadSelectedDictionary();
        }

        private void LoadSelectedDictionary()
        {
            if (File.Exists("selected_dictionary.txt"))
            {
                try
                {
                    MyDataBase.DBName = File.ReadAllText("selected_dictionary.txt");
                    MyDataBase.LoadDataToDataGridView("SELECT * FROM words;", dataGridView1);
                    DataGridViewHelper.HideColumnAndAddColumns(dataGridView1, 0, dataGridView1.ColumnCount, 
                        DataGridViewHelper.AddingButtonColumn(dataGridView1, "Change", "Изменить", 70, dataGridView1.ColumnCount),
                        DataGridViewHelper.AddingButtonColumn(dataGridView1, "Delete", "Удалить", 70, dataGridView1.ColumnCount));
                }
                catch (Exception)
                {
                    // Код сгенерирует исключение, если пользователь поменял что-то в selected_dictionary.txt
                    MyDataBase.CloseConnection();
                    File.Delete(MyDataBase.DBName);
                    File.Delete("selected_dictionary.txt");
                }
            }
        }

        private void добавитьСловоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddWord addWord = new AddWord(dataGridView1);
            addWord.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // Если нажата кнопка "Изменить"
            if (e.ColumnIndex + 1 == dataGridView1.ColumnCount - 1 && e.RowIndex >= 0)
            {
                ChangeWord changeWord = new ChangeWord(dataGridView1, dataGridView1.CurrentRow);
                changeWord.Show();
            }
            // Если нажата кнопка "Удалить"
            else if (e.ColumnIndex + 1 == dataGridView1.ColumnCount && e.RowIndex >= 0)
            {
                DataGridViewRow selectedRow = dataGridView1.CurrentRow;

                // Выводим на экран  word - transcription - translation,
                // начинаем с 1 так как 0 это id, его выводить не нужно.
                string message = string.Format("Вы уверены что хотите удалить слово?\n{0} - {1} - {2}",
                    selectedRow.Cells[1].Value,
                    selectedRow.Cells[2].Value,
                    selectedRow.Cells[3].Value
                    );

                DialogResult result = MessageBox.Show(message, "Удаление слова", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(result == DialogResult.Yes)
                {
                    MyDataBase.ExecuteQueryWithoutAnswer(string.Format("DELETE FROM words WHERE id = {0};", selectedRow.Cells[0].Value.ToString()));
                    MessageBox.Show("Запись удалена.");

                    MyDataBase.LoadDataToDataGridView("SELECT * FROM words;", dataGridView1);
                    DataGridViewHelper.HideColumnAndAddColumns(dataGridView1, 0, dataGridView1.ColumnCount,
                        DataGridViewHelper.AddingButtonColumn(dataGridView1, "Change", "Изменить", 70, dataGridView1.ColumnCount),
                        DataGridViewHelper.AddingButtonColumn(dataGridView1, "Delete", "Удалить", 70, dataGridView1.ColumnCount));
                }
            }
        }

        // Нажат кнопка "Поиск"
        private void button1_Click(object sender, EventArgs e)
        {
            if(MyDataBase.TestConnection())
            {
                string query = string.Format("SELECT * FROM words WHERE word LIKE '%{0}%' OR translation LIKE '%{0}%';", textBox1.Text);
                MyDataBase.LoadDataToDataGridView(query, dataGridView1);
                DataGridViewHelper.HideColumnAndAddColumns(dataGridView1, 0, dataGridView1.ColumnCount,
                   DataGridViewHelper.AddingButtonColumn(dataGridView1, "Change", "Изменить", 70, dataGridView1.ColumnCount),
                   DataGridViewHelper.AddingButtonColumn(dataGridView1, "Delete", "Удалить", 70, dataGridView1.ColumnCount));
            }
            else
            {
                MessageBox.Show("Вы должны выбрать словарь");
            }
        }

        private void редактироватьСловариToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditDictionaries editDictionaries = new EditDictionaries(dataGridView1);
            editDictionaries.Show();
        }

        private void выбратьСловарьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectDictionary selectDictionary = new SelectDictionary(dataGridView1);
            selectDictionary.Show();
        }

        private void категорииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditCategories editCategories = new EditCategories(dataGridView1);
            editCategories.Show();
        }

    }
}
