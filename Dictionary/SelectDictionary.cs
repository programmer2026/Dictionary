﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Dictionary
{
    public partial class SelectDictionary : Form
    {
        private Dictionaries dictionaries = new Dictionaries();
        private DataGridView dataGridViewForm1;

        public SelectDictionary()
        {
            InitializeComponent();
        }

        public SelectDictionary(DataGridView dataGridView) : this()
        {
            dataGridViewForm1 = dataGridView;
        }

        private void SelectDictionary_Load(object sender, EventArgs e)
        {
            if (File.Exists("dictionaries.json"))
            {
                MyJson.LoadDictionariesInListBox(ref dictionaries, listBox1);
            }
            else
            {
                MessageBox.Show("Перед выбором словаря, вы должны создать хотя б один.");
                Close();
            }

            if (File.Exists("selected_dictionary.txt"))
            {
                string selectedDictionary = File.ReadAllText("selected_dictionary.txt");
                int itemIndex = listBox1.FindString(selectedDictionary.Remove(selectedDictionary.Length - 3, 3).Replace("-", " - "));
                if (itemIndex >= 0)
                {
                    listBox1.SetSelected(itemIndex, true);
                }
                else
                {
                    // Если пользователь что-то изменил в файле, то мы удаляем этот файл
                    File.Delete("selected_dictionary.txt");
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedItem != null)
            {
                MyDataBase.DBName = StringHelper.GenerateFileName(listBox1.SelectedItem.ToString(), ".db");

                MyDataBase.LoadDataToDataGridView("SELECT * FROM words;", dataGridViewForm1);
                DataGridViewHelper.HideColumnAndAddColumns(dataGridViewForm1, 0, dataGridViewForm1.ColumnCount,
                       DataGridViewHelper.AddingButtonColumn(dataGridViewForm1, "Change", "Изменить", 70, dataGridViewForm1.ColumnCount),
                       DataGridViewHelper.AddingButtonColumn(dataGridViewForm1, "Delete", "Удалить", 70, dataGridViewForm1.ColumnCount));

                SaveNameOfSelectedDictionary();
                Close();
            }
            else MessageBox.Show("Вы должны выбрать словарь");
        }

        private void SaveNameOfSelectedDictionary()
        {
            File.WriteAllText("selected_dictionary.txt", MyDataBase.DBName);
        }
    }
}
