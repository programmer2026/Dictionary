﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;
using System.Data;
using System.IO;

namespace Dictionary
{
    static class MyDataBase
    {
        
        private static SQLiteConnection connection;
        private static SQLiteCommand command;

        public static string DBName { get; set; }

        private static void OpenConnection()
        {
            connection = new SQLiteConnection("Data Source = " + DBName);
            command = new SQLiteCommand(connection);
            connection.Open();
        }
        public static void CloseConnection()
        {
            connection.Close();
            command.Dispose();
        }
        public static bool TestConnection()
        {
            if (File.Exists(DBName)) return true;
            return false;
        }

        public static void ExecuteQueryWithoutAnswer(string query)
        {
            OpenConnection();

            command.CommandText = query;
            command.ExecuteNonQuery();

            CloseConnection();
        }
        // Возращает значение первой записи первого столбца вызванной таблицы
        public static string ExecuteQueryWithAnswer(string query)
        {
            OpenConnection();

            string answer = string.Empty;
            command.CommandText = query;
            answer = command.ExecuteScalar().ToString();

            CloseConnection();

            return answer;
        }

        public static DataTable GetTable(string query)
        {
            OpenConnection();

            SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, connection);

            DataSet DS = new DataSet();
            adapter.Fill(DS);
            adapter.Dispose();

            CloseConnection();

            return DS.Tables[0];
        }
        public static void LoadDataToDataGridView(string query, DataGridView dataGridView)
        {
            dataGridView.Columns.Clear();
            dataGridView.DataSource = null;
            dataGridView.DataSource = GetTable(query);
        }
        // Для добавления нового слова или изменения текущего нужно заполнить dataGridView.
        // Данный метод загружает колонки и пустую строку для ввода текста.
        public static void LoadColumnsInDataGridView(string nameTable, DataGridView dataGridView)
        {
            dataGridView.Columns.Clear();
            dataGridView.DataSource = null;

            // Копируем структуру таблицы (столбцы)
            DataTable DT = GetTable($"PRAGMA table_info({nameTable});");

            for (int i = 0; i < DT.Columns.Count; i++)
            {
                dataGridView.Columns.Add("column" + i, DT.Rows[i][1].ToString());
            }
        }
        public static void LoadCategoryInComboBoxColumn(string query, DataGridViewComboBoxColumn comboBoxColumn)
        {
            DataTable DT = GetTable(query);
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                comboBoxColumn.Items.Add(DT.Rows[i][1].ToString());
            } 
        }
    }
}
