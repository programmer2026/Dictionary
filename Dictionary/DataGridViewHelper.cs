﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    static class DataGridViewHelper
    {
        /// <summary>
        /// Добавляет ButtonColumn в DataGridView.
        /// </summary>
        public static DataGridViewButtonColumn AddingButtonColumn(DataGridView dataGridView, string headerText, string text, int width, int index = 0)
        {
            DataGridViewButtonColumn buttonColumn = new DataGridViewButtonColumn();

            buttonColumn.HeaderText = headerText;
            buttonColumn.Name = "buttonColumn";
            buttonColumn.Text = text;
            buttonColumn.UseColumnTextForButtonValue = true;

            dataGridView.Columns.Insert(index, buttonColumn);
            dataGridView.Columns[dataGridView.ColumnCount - 1].Width = width;

            return buttonColumn;
        }

        /// <summary>
        /// Добавляет ComboBoxColumn в DataGridView.
        /// </summary>
        public static DataGridViewComboBoxColumn AddingComboBoxColumn(DataGridView dataGridView, string headerText, int width, int index = 0)
        {
            DataGridViewComboBoxColumn comboBoxColumn = new DataGridViewComboBoxColumn();

            comboBoxColumn.HeaderText = headerText;
            comboBoxColumn.Name = "comboBoxColumn";

            dataGridView.Columns.Insert(index, comboBoxColumn);
            dataGridView.Columns[dataGridView.ColumnCount - 1].Width = width;

            return comboBoxColumn;
        }

        /// <summary>
        /// Добавляет ComboBoxColumn в DataGridView и добавляет значения переданные в списке items.
        /// </summary>
        public static DataGridViewComboBoxColumn AddingComboBoxColumn(DataGridView dataGridView, string headerText, int width, List<string> items, int index = 0)
        {
            return LoadValuesInComboBox(AddingComboBoxColumn(dataGridView, headerText, width, index), items);
        }
        private static DataGridViewComboBoxColumn LoadValuesInComboBox(DataGridViewComboBoxColumn comboBoxColumn, List<string> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                comboBoxColumn.Items.Add(items[i]);
            }
            return comboBoxColumn;
        }

        /// <summary>
        /// Проверяет заданную строку с заданного столбца на наличие пустых ячеек, начиная с startIndexOfCell.
        /// </summary>
        public static bool isThereEmptyCellInRow(DataGridView dataGridView, int row, int startIndexOfCell)
        {
            for (int i = startIndexOfCell; i < dataGridView.ColumnCount; i++)
            {
                if (dataGridView.Rows[row].Cells[i].Value == null || dataGridView.Rows[row].Cells[i].Value.ToString() == string.Empty)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Формираем строку состоящую из значений ячеек указаного ряда, разделённых указанной строкой, начиная с startIndexOfCell.
        /// </summary>
        /// <exception cref="NullReferenceException">Выдаёт когда dataGridView = null</exception>
        public static string ToFromValues(DataGridView dataGridView, int row, int startIndexOfCell, string separatorString)
        {
            string str = "";
            for (int i = startIndexOfCell; i < dataGridView.ColumnCount; i++)
            {
                str += $"'{dataGridView.Rows[row].Cells[i].Value.ToString()}'{separatorString}";
            }
            str = str.Remove(str.Length - 2, 2);
            return str;
        }

        /// <summary>
        /// Скрывает колонку
        /// </summary>
        public static void HideColumn(DataGridView dataGridView, int hiddenColumnIndex)
        {
            dataGridView.Columns[hiddenColumnIndex].Visible = false;
        }

        /// <summary>
        /// Скрывает колонку и добавляет колонки переданые в params.
        /// Если колонка уже существует, то она не будет добавлена.
        /// </summary>
        public static void HideColumnAndAddColumns(DataGridView dataGridView, int hiddenColumnIndex, int columnIndex, params DataGridViewColumn[] columns)
        {
            HideColumn(dataGridView, hiddenColumnIndex);
            for (int i = 0; i < columns.Length; i++)
            {
                if(!dataGridView.Columns.Contains(columns[i]))
                    dataGridView.Columns.Insert(columnIndex + i, columns[i]);
            }
        }

    }
}
