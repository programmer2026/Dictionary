﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class EditCategories : Form
    {
        private DataGridView dataGridViewForm1;

        public EditCategories()
        {
            InitializeComponent();
        }

        public EditCategories(DataGridView dataGridView) : this()
        {
            dataGridViewForm1 = dataGridView;
        }

        private void EditCategories_Load(object sender, EventArgs e)
        {
            if(MyDataBase.TestConnection())
            {
                MyDataBase.LoadDataToDataGridView("SELECT * FROM categories;", dataGridView1);
                DataGridViewHelper.HideColumn(dataGridView1, 0);
            }
            else
            {
                MessageBox.Show("Вы должны выбрать словарь.");
                Close();
            }
        }

        // Добавить категорию
        private void AddCategoryButton_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != string.Empty)
            {
                string answer = MyDataBase.ExecuteQueryWithAnswer("SELECT MAX(id) FROM words;");
                int maxID = answer == string.Empty ? 0 : int.Parse(answer) + 1;

                MyDataBase.ExecuteQueryWithoutAnswer($"INSERT INTO categories VALUES ({maxID}, '{textBox1.Text}')");
                MyDataBase.LoadDataToDataGridView("SELECT * FROM categories;", dataGridView1);
                DataGridViewHelper.HideColumn(dataGridView1, 0);
            }
            else MessageBox.Show("Вы должны ввести имя категории в поле ввода.");
        }

        // Удалить категорию
        private void DeleteCategoryButton_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                if (dataGridView1.CurrentRow.Cells[1].Value.ToString() != "Общая")
                {
                    DialogResult result = MessageBox.Show("Вы действительно хотите удалить категорию? Если вы её удалите, то все слова которые относились к этой категории будут иметь категорию \"Общая\"", "Удаление категории", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        MyDataBase.ExecuteQueryWithoutAnswer($"UPDATE words SET category = 'Общая' WHERE category = '{dataGridView1.CurrentRow.Cells[1].Value.ToString()}';");
                        MyDataBase.ExecuteQueryWithoutAnswer($"DELETE FROM categories WHERE id = {dataGridView1.CurrentRow.Cells[0].Value.ToString()};");

                        MyDataBase.LoadDataToDataGridView("SELECT * FROM categories;", dataGridView1);
                        DataGridViewHelper.HideColumn(dataGridView1, 0);

                        MyDataBase.LoadDataToDataGridView("SELECT * FROM words;", dataGridViewForm1);
                        DataGridViewHelper.HideColumnAndAddColumns(dataGridViewForm1, 0, dataGridViewForm1.ColumnCount,
                            DataGridViewHelper.AddingButtonColumn(dataGridViewForm1, "Change", "Изменить", 70, dataGridViewForm1.ColumnCount),
                            DataGridViewHelper.AddingButtonColumn(dataGridViewForm1, "Delete", "Удалить", 70, dataGridViewForm1.ColumnCount));
                    }
                }
                else MessageBox.Show("Вы не можете удалить категорию \"Общая\"");
            }
            else MessageBox.Show("Вы должны выбрать категорию, которую нужно удалить.");
        }
    }
}
