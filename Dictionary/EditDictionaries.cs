﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Dictionary
{
    public partial class EditDictionaries : Form
    {
        private enum Languages
        {
            Русский,
            Український,
            English,
            Беларускі,
            Қазақша,
            中文_繁體,
            中文_簡體,
            한국어,
            日本,
            Latine,
            Deutsch,
            Polski,
            Français,
            Español,
            Italiano
        }

        private Dictionaries dictionaries = new Dictionaries();

        private DataGridView dataGridView;

        public EditDictionaries()
        {
            InitializeComponent();
        }

        public EditDictionaries(DataGridView dataGridView) : this()
        {
            this.dataGridView = dataGridView;
        }

        private void EditDictionaries_Load(object sender, EventArgs e)
        {
            LoadLanguagesInComboBox(comboBox1);
            LoadLanguagesInComboBox(comboBox2);

            comboBox1.SelectedIndex = (int)Languages.English;
            comboBox2.SelectedIndex = (int)Languages.Русский;

            if (File.Exists("dictionaries.json"))
            {
                MyJson.LoadDictionariesInListBox(ref dictionaries, listBox1);
            }
        }

        private void LoadLanguagesInComboBox(ComboBox comboBox)
        {
            int countLanguages = Enum.GetNames(typeof(Languages)).Length;
            for (int i = 0; i < countLanguages; i++)
            {
                comboBox.Items.Add((Languages)i);
            }
        }

        // Добавить словарь
        private void AddDictionary_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != comboBox2.Text)
            {
                string newItem = string.Format("{0} - {1}", comboBox1.Text, comboBox2.Text);
                if (!listBox1.Items.Contains(newItem))
                {
                    string nameDataBase = StringHelper.GenerateFileName(newItem, ".db"); ;
                    CreateDataBase(nameDataBase);

                    dictionaries.items.Add(newItem);
                    listBox1.Items.Add(newItem);

                    MyJson.SaveDictionaries(dictionaries);
                }
                else MessageBox.Show("Данный словарь уже есть.");
            }
            else MessageBox.Show("Вы должны указать разные языки.");
        }

        private void CreateDataBase(string nameDataBase)
        {
            MyDataBase.DBName = nameDataBase;

            MyDataBase.ExecuteQueryWithoutAnswer("CREATE TABLE words " +
                "(" +
                "id INT, " +
                "word VARCHAR(50), " +
                "transcription VARCHAR(50), " +
                "translation VARCHAR(50), " +
                "example VARCHAR(100)," +
                "category VARCHAR(100)" +
                ");");
            MyDataBase.ExecuteQueryWithoutAnswer("CREATE TABLE categories " +
                "(" +
                "id INT, " +
                "category VARCHAR(100)" +
                ");");
            MyDataBase.ExecuteQueryWithoutAnswer("INSERT INTO categories VALUES (0, 'Общая')");
        }

        // Удалить словарь
        private void DeleteDictionary_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                DialogResult result = MessageBox.Show($"Вы дествительно хотите удалить словарь {listBox1.SelectedItem.ToString()} ?", "Удаление словаря", MessageBoxButtons.YesNo);

                if (result == DialogResult.Yes)
                {
                    string nameDataBase = StringHelper.GenerateFileName(listBox1.SelectedItem.ToString(), ".db");
                    DeleteDataBase(nameDataBase);

                    if (nameDataBase == MyDataBase.DBName)
                    {
                        dataGridView.DataSource = null;
                        dataGridView.Columns.Clear();
                    }

                    dictionaries.items.Remove(listBox1.SelectedItem.ToString());
                    listBox1.Items.Remove(listBox1.SelectedItem);

                    if (listBox1.Items.Count == 0) File.Delete("dictionaries.json");
                    else MyJson.SaveDictionaries(dictionaries);
                }
            }
        }

        private void DeleteDataBase(string nameDataBase)
        {
            File.Delete(nameDataBase);
        }
    }
}
