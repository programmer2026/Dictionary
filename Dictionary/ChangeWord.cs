﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dictionary
{
    public partial class ChangeWord : Form
    {
        private DataGridView dataGridViewForm1;
        // Ряд который нужно изменить, нужен для того, 
        // что б загрузить значения с этого ряда в dataGridView данной формы
        private DataGridViewRow dataGridViewRowForm1;

        public ChangeWord()
        {
            InitializeComponent();
        }

        public ChangeWord(DataGridView dataGridView, DataGridViewRow dataGridViewRow) : this()
        {
            dataGridViewForm1 = dataGridView;
            dataGridViewRowForm1 = dataGridViewRow;
        }

        private void ChangeWord_Load(object sender, EventArgs e)
        {
            LoadRowInDataGridView(dataGridView1, dataGridViewRowForm1);
        }

        private void LoadRowInDataGridView(DataGridView dataGridView, DataGridViewRow dataGridViewRow)
        {
            MyDataBase.LoadColumnsInDataGridView("words", dataGridView);
            // Удаляем колонку Category 
            dataGridView1.Columns.Remove(dataGridView1.Columns[dataGridView1.Columns.Count - 1]);
            // Добавляем одну строку в DataTable для того, что б в неё можно было записать новое слово или изменить текущее.
            dataGridView1.Rows.Add();
            DataGridViewHelper.HideColumn(dataGridView, 0);
            MyDataBase.LoadCategoryInComboBoxColumn("SELECT * FROM categories;",
                DataGridViewHelper.AddingComboBoxColumn(dataGridView, "category", 100, dataGridView.ColumnCount));

            for (int i = 0; i < dataGridView.ColumnCount; i++)
            {
                dataGridView.Rows[0].Cells[i].Value = dataGridViewRow.Cells[i].Value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Мы передаем startIndexOfCell = 1, а не 0, потому что 1 столбец это id, 
            // а мы его не даём вводить пользователю
            if (!DataGridViewHelper.isThereEmptyCellInRow(dataGridView1, 0, 1))
            {
                DataGridViewRow firstRow = dataGridView1.Rows[0];

                DialogResult result = MessageBox.Show("Вы уверены что хотите изменить слово?", "Изменение слова", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    MyDataBase.ExecuteQueryWithoutAnswer("UPDATE words SET " +
                       $"word = '{firstRow.Cells[1].Value}', " +
                       $"transcription = '{firstRow.Cells[2].Value}', " +
                       $"translation = '{firstRow.Cells[3].Value}', " +
                       $"example = '{firstRow.Cells[4].Value}', " +
                       $"category = '{firstRow.Cells[5].Value}' " +
                       $"WHERE id = {firstRow.Cells[0].Value};"
                       );

                    MyDataBase.LoadDataToDataGridView("SELECT * FROM words;", dataGridViewForm1);
                    DataGridViewHelper.HideColumnAndAddColumns(dataGridViewForm1, 0, dataGridViewForm1.ColumnCount,
                      DataGridViewHelper.AddingButtonColumn(dataGridViewForm1, "Change", "Изменить", 70, dataGridViewForm1.ColumnCount),
                      DataGridViewHelper.AddingButtonColumn(dataGridViewForm1, "Delete", "Удалить", 70, dataGridViewForm1.ColumnCount));

                    MessageBox.Show("Слово успешно изменено!");

                    Close();
                }
            }
            else
            {
                MessageBox.Show("Все ячейки должны быть заполнены!");
            }
        }
    }
}
