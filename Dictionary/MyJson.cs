﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace Dictionary
{
    static class MyJson
    {
        public static void LoadDictionariesInListBox(ref Dictionaries dictionaries, ListBox listBox)
        {
            dictionaries = JsonConvert.DeserializeObject<Dictionaries>(File.ReadAllText("dictionaries.json"));

            for (int i = 0; i < dictionaries.items.Count; i++)
            {
                listBox.Items.Add(dictionaries.items[i]);
            }
        }
        public static void SaveDictionaries(Dictionaries dictionaries)
        {
            File.WriteAllText("dictionaries.json", JsonConvert.SerializeObject(dictionaries));
        }
    }
}
